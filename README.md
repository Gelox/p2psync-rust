p2psync is meant to function as a peer-to-peer substitute for cloud based 
real-time file synchronization tools.
It aspires to create a server-like functionality that can be used to build 
light-weight tools upon that allow for easy real-time integration of file 
synchronization by editors and similar tools.