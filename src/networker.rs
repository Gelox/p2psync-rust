use crate::errors::{Error, ErrorKind, Result};
use crate::logger::{log, log_err};
use crate::network_types::{NetworkCommand, NetworkContact, NetworkMessage, NetworkResponse};
use async_trait::async_trait;
use futures::stream::SelectAll;
use log::info;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::net::SocketAddr;
use tokio::fs::File;
use tokio::net::ToSocketAddrs;
use tokio::prelude::*;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::oneshot;
use tokio::{
    net::{TcpListener, TcpStream},
    stream::Stream,
    stream::StreamExt,
};

const CHANNEL_SIZE: usize = 100;

//Internal enum for sending messages, should never the public and might be removed later
#[derive(Debug)]
enum PrivNetworkCommand {
    SendMessage(NetworkMessage),
}

///Sends and receives networking messages
#[async_trait]
pub trait INetworker: Sized + Send {
    ///Sends a message to a contact
    async fn send_message(&mut self, msg: NetworkMessage, to: &NetworkContact) -> Result<()>;
    ///Returns a channel which can be listened for network responses, if one is available
    async fn recieve_channel(&mut self) -> Option<Receiver<NetworkResponse>>;
}

//Delimiter that separates messages from each other in the tcp stream
const DELIMITER: &str = "|";

async fn connection_thread<S: SocketStream>(
    mut sender: Sender<NetworkResponse>,
    mut com_chan: Receiver<PrivNetworkCommand>,
    mut stream: S,
    contact: NetworkContact,
) -> Result<()> {
    let mut buf = [0; 1500];
    log(format!("New connection thread"));
    loop {
        tokio::select! {
            Ok(n) = stream.read(&mut buf) => {
                let buf = match String::from_utf8(buf[..n].to_vec()) {
                    Ok(buf) => buf,
                    Err(e) => return Err(Error::new(ErrorKind::NetworkError, e)),
                };
                let mut split: Vec<&str> = buf.split(DELIMITER).collect();
                split.pop();
                for buf in split {
                    let buf: NetworkMessage = match serde_yaml::from_str(&buf) {
                        Ok(buf) => buf,
                        Err(e) => {
                            log_err(format!("Networker::connection_thread: could not deserialize {:?}\nbuf = {:?}\nbuf_size = {}", e, buf, n));
                            continue;
                        },
                    };
                    sender.send(NetworkResponse::ReadMessage(buf, contact.clone())).await?;
                }
            }
            Some(command) = com_chan.recv() => {
                log(format!("Got a command {:?}", command));
                match command {
                    PrivNetworkCommand::SendMessage(msg) => {
                        let mut msg = match serde_yaml::to_string(&msg) {
                            Ok(msg) => msg,
                            Err(e) => {
                                log_err(format!("Networker::connection_thread: could not serialize {:?}", e));
                                continue;
                            },
                        };
                        //FIXME Any message that is sent that contains DELIMITER will be corrupted and
                        //will not be deserialized
                        msg.push_str(DELIMITER);
                        stream.write_all(msg.as_bytes()).await?;
                        log(format!("Wrote message"));
                    }
                }
            }
        }
    }
}

pub mod dns {
    use super::*;
    const DNS_CACHE_FILE_NAME: &str = ".dns_cache.yml";
    #[async_trait]
    pub trait DNSLookup: Send + Sync {
        async fn dns_lookup(&mut self, contact: &NetworkContact) -> Result<SocketAddr>;
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    struct DNSCache {
        table: HashMap<String, SocketAddr>,
    }
    impl DNSCache {
        fn new() -> Self {
            DNSCache {
                table: HashMap::new(),
            }
        }
    }

    pub struct CachingDNS {
        memory_cache: HashMap<String, SocketAddr>,
    }

    impl CachingDNS {
        pub fn with(cached: Vec<(String, SocketAddr)>) -> Self {
            let mut memory_cache = HashMap::new();
            for (id, addr) in cached {
                if let None = memory_cache.insert(id, addr) {}
            }
            Self { memory_cache }
        }
    }

    #[async_trait]
    impl DNSLookup for CachingDNS {
        //TODO: This should be changed so it can also access an internet login server that functions as a
        //DNS
        //This should be moved into a separate file
        //It can also be updated so it uses a faster database rather than a yml file which is read and
        //turned into a hashmap every time the function is called.
        async fn dns_lookup(&mut self, contact: &NetworkContact) -> Result<SocketAddr> {
            info!("Querying DNS for {:?}", contact);
            if let Some(addr) = self.memory_cache.get(&contact.username) {
                return Ok(addr.clone());
            }
            let mut cache_file = match File::open(DNS_CACHE_FILE_NAME).await {
                Ok(f) => f,
                Err(_) => match File::create(DNS_CACHE_FILE_NAME).await {
                    Ok(mut f) => {
                        let cache = DNSCache::new();
                        let content = serde_yaml::to_string(&cache).unwrap();
                        f.write_all(&content.into_bytes()).await?;
                        f.sync_all().await?;
                        f
                    }
                    Err(e) => return Err(Error::new(ErrorKind::DNSError, e)),
                },
            };
            let mut content = String::new();
            cache_file.read_to_string(&mut content).await?;
            let mut cache: DNSCache = match serde_yaml::from_str(&content) {
                Ok(cache) => cache,
                Err(_) => {
                    return Err(Error::new(
                        ErrorKind::DNSError,
                        "Can not read DNS cache format",
                    ))
                }
            };

            if let Some(addr) = cache.table.remove(&contact.username) {
                Ok(addr)
            } else {
                Err(Error::new(
                    ErrorKind::DNSError,
                    "Could not find user in DNS cache",
                ))
            }
        }
    }
}

///Handshake should contain the contact of the peer that sends the handshake
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
struct Handshake {
    contact: NetworkContact,
}

impl Handshake {
    fn new(contact: NetworkContact) -> Self {
        Handshake { contact }
    }
}

///Implementation of the `INetworker` trait
#[derive(Debug)]
pub struct Networker {
    command_channel: Sender<(NetworkCommand, oneshot::Sender<Result<()>>)>,
    recv: Option<Receiver<NetworkResponse>>,
}

#[async_trait]
impl INetworker for Networker {
    async fn send_message(&mut self, msg: NetworkMessage, to: &NetworkContact) -> Result<()> {
        let (oneshot_tx, oneshot_rx) = oneshot::channel();
        if let Err(e) = self
            .command_channel
            .send((NetworkCommand::SendMessage(msg, to.clone()), oneshot_tx))
            .await
        {
            return Err(Error::new(ErrorKind::NetworkError, e));
        }
        match oneshot_rx.await {
            Ok(r) => r,
            Err(e) => Err(Error::new(ErrorKind::NetworkError, e)),
        }
    }
    async fn recieve_channel(&mut self) -> Option<Receiver<NetworkResponse>> {
        match self.recv {
            Some(_) => self.recv.take(),
            None => None,
        }
    }
}

type NetCommandOneshot = (NetworkCommand, oneshot::Sender<Result<()>>);
impl Networker {
    async fn accept_handshake<S: SocketStream>(mut stream: S) -> Result<(NetworkContact, S)> {
        let mut buf = [0; 1500];
        match stream.read(&mut buf).await {
            Ok(n) => {
                let buf = match String::from_utf8(buf[..n].to_vec()) {
                    Ok(buf) => buf,
                    Err(e) => return Err(Error::new(ErrorKind::NetworkError, e)),
                };
                let mut split: Vec<&str> = buf.split(DELIMITER).collect();
                split.pop();
                for buf in split {
                    let handshake: Handshake = serde_yaml::from_str(&buf)?;
                    return Ok((handshake.contact, stream));
                }
                return Err(Error::new(ErrorKind::NetworkError, "Handshake invalid"));
            }
            Err(e) => Err(Error::with_msg(
                ErrorKind::NetworkError,
                e,
                "Handshake invalid, could not read from stream".to_owned(),
            )),
        }
    }

    async fn send_handshake<S: SocketStream>(mut stream: S) -> Result<S> {
        let contact = NetworkContact::me();
        let mut msg = serde_yaml::to_string(&Handshake::new(contact))?;
        msg.push_str(DELIMITER);
        stream.write_all(msg.as_bytes()).await?;
        log(format!("Sent handshake"));
        Ok(stream)
    }

    async fn accept_connection<S: SocketStream + 'static>(
        stream: S,
        rx_chans: &mut SelectAll<Receiver<NetworkResponse>>,
        tx_chans: &mut HashMap<NetworkContact, Sender<PrivNetworkCommand>>,
    ) -> Result<()> {
        match Self::accept_handshake(stream).await {
            Ok((contact, stream)) => {
                let (tx, rx) = channel(CHANNEL_SIZE);
                let (tx2, rx2) = channel(CHANNEL_SIZE);
                rx_chans.push(rx);
                log(format!(
                    "Someone else is trying to connect ... {:?}",
                    contact
                ));
                tx_chans.insert(contact.clone(), tx2);
                tokio::spawn(connection_thread(tx, rx2, stream, contact));
                Ok(())
            }
            Err(e) => {
                log(format!("Could not accept handshake: {:?}", e));
                return Err(e);
            }
        }
    }

    async fn send_message<
        F: SocketFactory<L, S> + 'static,
        L: SocketListener<S>,
        S: SocketStream + 'static,
    >(
        msg: NetworkMessage,
        to: NetworkContact,
        dns: &mut (impl dns::DNSLookup + 'static),
        tx_chans: &mut HashMap<NetworkContact, Sender<PrivNetworkCommand>>,
        rx_chans: &mut SelectAll<Receiver<NetworkResponse>>,
        sf: &mut F,
    ) -> Result<()> {
        //FIXME: There is some bug where the first save of the file the DNS
        //is looked up but not message is forwarded, if the file is saved
        //again then the message is forwarded.
        let address = match dns.dns_lookup(&to).await {
            Ok(addr) => addr,
            Err(e) => {
                return Err(e);
            }
        };
        log(format!("Trying to send a message to...{}", address));
        if let Some(sender) = tx_chans.get_mut(&to) {
            if let Err(e) = sender.send(PrivNetworkCommand::SendMessage(msg)).await {
                log_err(format!("Could not send to thread: {:?}", e));
            }
        } else {
            log(format!("Trying to connect..."));
            if let Ok(stream) = sf.connect(address).await {
                log(format!("Sending handshake..."));
                let stream = Self::send_handshake(stream).await.unwrap();
                let (tx, rx) = channel(CHANNEL_SIZE);
                let (tx2, rx2) = channel(CHANNEL_SIZE);
                rx_chans.push(rx);
                log(format!("Spawning...{:?}", to));
                tokio::spawn(connection_thread(tx, rx2, stream, to.clone()));
                log(format!("Inserting {:?} into {:?}", tx2, to));
                if let None = tx_chans.insert(to.clone(), tx2) {
                    let sender = tx_chans.get_mut(&to).unwrap();
                    if let Err(e) = sender.send(PrivNetworkCommand::SendMessage(msg)).await {
                        log_err(format!("Could not send to thread: {:?}", e));
                    }
                }
            }
        }
        Ok(())
    }

    pub async fn run<
        F: SocketFactory<L, S> + 'static,
        L: SocketListener<S>,
        S: SocketStream + 'static,
    >(
        mut dns: impl dns::DNSLookup + 'static,
        mut sf: F,
    ) -> Self {
        let (command_sender, mut command_chan): (
            Sender<NetCommandOneshot>,
            Receiver<NetCommandOneshot>,
        ) = channel(CHANNEL_SIZE);
        let (mut tx, rx) = channel(CHANNEL_SIZE);
        tokio::spawn(async move {
            let mut tx_chans: HashMap<NetworkContact, Sender<PrivNetworkCommand>> = HashMap::new();
            let mut rx_chans: SelectAll<Receiver<NetworkResponse>> = SelectAll::new();
            let mut listener = sf.bind("127.0.0.1:8080").await.unwrap();
            loop {
                tokio::select! {
                    //Some socket stream is connecting to us
                    Some(stream) = listener.next() => {
                        match stream {
                            Ok(stream) => {
                                match Self::accept_connection(stream, &mut rx_chans, &mut tx_chans).await {
                                    Ok(_) => (),
                                    Err(e) => {
                                        log(format!("Could not accept connection: {:?}", e));
                                    }
                                }
                            }
                            Err(e) => {
                                log(format!("Could not get connecting stream: {:?}", e));
                            },
                        }
                    }
                    //A command has been recieved
                    Some((command, result_sender)) = command_chan.recv() => {
                        match command {
                            NetworkCommand::SendMessage(msg, to) => {
                                result_sender
                                    .send(Self::send_message(msg, to, &mut dns, &mut tx_chans, &mut rx_chans, &mut sf).await)
                                    .unwrap();
                            }
                        }
                    }
                    //A message has been recieved
                    Some(response) = rx_chans.next() => {
                        match response {
                            NetworkResponse::ReadMessage(msg, from) => {
                                log(format!("Got a message {:?}", msg));
                                if let Err(e) = tx.send(NetworkResponse::ReadMessage(msg, from)).await {
                                    log(format!("Could not send to thread: {:?}", e));
                                }
                            }
                        }
                    }
                    //The select panics with an "internal error" if all branches are disabled, we
                    //put this here to exit the networker
                    else =>  {
                        panic!("All networker branches are disabled, no longer listening for new connections");
                    }
                }
            }
        });
        Networker {
            command_channel: command_sender,
            recv: Some(rx),
        }
    }
}

#[async_trait]
pub trait SocketStream: AsyncRead + AsyncWrite + Unpin + Send + Sync {}

#[async_trait]
pub trait SocketListener<S: SocketStream>:
    Stream<Item = std::io::Result<S>> + Unpin + Send + Sync
{
}

impl SocketStream for TcpStream {}

impl SocketListener<TcpStream> for TcpListener {}

#[async_trait]
pub trait SocketFactory<L: SocketListener<S>, S: SocketStream>: Send + Sync {
    async fn bind<A: ToSocketAddrs + Send + Sync>(&mut self, addr: A) -> Result<L>;
    async fn connect<A: ToSocketAddrs + Send + Sync>(&mut self, addr: A) -> Result<S>;
}

pub struct TcpSocketFactory {}

impl TcpSocketFactory {
    pub fn new() -> Self {
        TcpSocketFactory {}
    }
}

#[async_trait]
impl SocketFactory<TcpListener, TcpStream> for TcpSocketFactory {
    async fn bind<A: ToSocketAddrs + Send + Sync>(&mut self, addr: A) -> Result<TcpListener> {
        TcpListener::bind(addr).await.map_err(|e| Error::from(e))
    }
    async fn connect<A: ToSocketAddrs + Send + Sync>(&mut self, addr: A) -> Result<TcpStream> {
        TcpStream::connect(addr).await.map_err(|e| Error::from(e))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::mocks::MockSocketFactoryTwins;
    use crate::node_config::NodeID;
    use dns::CachingDNS;
    use std::net::{IpAddr, Ipv4Addr};
    use std::time::Duration;
    use tokio::time::timeout;
    const TEST_TIMEOUT_MICROS: u64 = 1000;

    #[test]
    fn recieve_message() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let my_ip = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8000);
            let networker_ip = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 2)), 8001);
            let (networker_sf, mut my_sf) =
                MockSocketFactoryTwins::new(networker_ip.clone(), my_ip);
            let dns = CachingDNS::with(vec![]);
            let mut networker = Networker::run(dns, networker_sf).await;
            let mut stream = my_sf.connect(networker_ip).await.unwrap();

            let contact = NetworkContact::new("outside_contact".to_owned());
            let mut msg = serde_yaml::to_string(&Handshake::new(contact.clone())).unwrap();
            msg.push_str(DELIMITER);
            stream.write_all(msg.as_bytes()).await.unwrap();
            let node_id = NodeID::new();
            let accept_msg = NetworkMessage::new_accept(contact, node_id);
            let mut msg = serde_yaml::to_string(&accept_msg).unwrap();
            msg.push_str(DELIMITER);
            stream.write_all(msg.as_bytes()).await.unwrap();

            let mut rx = networker.recieve_channel().await.unwrap();
            let m = timeout(Duration::from_micros(TEST_TIMEOUT_MICROS), rx.recv())
                .await
                .unwrap()
                .unwrap();
            match m {
                NetworkResponse::ReadMessage(msg, _from) => {
                    if msg != accept_msg {
                        panic!();
                    }
                }
            }
        });
    }

    #[test]
    fn recieve_message_no_handshake() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let my_ip = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8000);
            let networker_ip = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 2)), 8001);
            let (networker_sf, mut my_sf) =
                MockSocketFactoryTwins::new(networker_ip.clone(), my_ip);
            let dns = CachingDNS::with(vec![]);
            let mut networker = Networker::run(dns, networker_sf).await;
            let mut stream = my_sf.connect(networker_ip).await.unwrap();

            let contact = NetworkContact::new("outside_contact".to_owned());
            let node_id = NodeID::new();
            let accept_msg = NetworkMessage::new_accept(contact, node_id);
            let mut msg = serde_yaml::to_string(&accept_msg).unwrap();
            msg.push_str(DELIMITER);
            stream.write_all(msg.as_bytes()).await.unwrap();

            let mut rx = networker.recieve_channel().await.unwrap();
            let m = timeout(Duration::from_micros(TEST_TIMEOUT_MICROS), rx.recv()).await;
            match m {
                Ok(_) => panic!(),
                Err(e) => match e {
                    tokio::time::Elapsed { .. } => {}
                },
            }
        });
    }

    #[test]
    fn send_message() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let my_ip = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8000);
            let networker_ip = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 2)), 8001);
            let (networker_sf, mut my_sf) =
                MockSocketFactoryTwins::new(networker_ip.clone(), my_ip);
            let contact = NetworkContact::new("me");
            let dns = CachingDNS::with(vec![("me".to_owned(), my_ip.clone())]);
            let mut networker = Networker::run(dns, networker_sf).await;
            let mut my_listener = my_sf.bind(my_ip).await.unwrap();

            let node_id = NodeID::new();
            let accept_msg = NetworkMessage::new_accept(contact.clone(), node_id);
            networker
                .send_message(accept_msg.clone(), &contact)
                .await
                .unwrap();
            let mut socket = my_listener.next().await.unwrap().unwrap();

            let mut buf = [0; 1500];
            let n = timeout(Duration::from_micros(1000), socket.read(&mut buf))
                .await
                .unwrap()
                .unwrap();
            let handshake = Handshake::new(NetworkContact::me());
            let buf = String::from_utf8(buf[0..n].to_vec()).unwrap();
            let buf = buf.split(DELIMITER).collect::<Vec<&str>>();
            let first = buf[0];
            let r_handshake: Handshake = serde_yaml::from_str(&first).unwrap();
            assert_eq!(r_handshake, handshake);
            let mut buf = [0; 1500];
            let n = timeout(Duration::from_micros(1000), socket.read(&mut buf))
                .await
                .unwrap()
                .unwrap();
            let buf = String::from_utf8(buf[0..n].to_vec()).unwrap();
            let buf = buf.split(DELIMITER).collect::<Vec<&str>>()[0];
            let r_msg: NetworkMessage = serde_yaml::from_str(&buf).unwrap();
            assert_eq!(r_msg, accept_msg);
        });
    }

    #[test]
    fn networkers_send_message_over_mock_sockets() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let ip_one = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8000);
            let ip_two = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 2)), 8001);
            let (sf_one, sf_two) = MockSocketFactoryTwins::new(ip_one, ip_two);
            let dns = CachingDNS::with(vec![(String::from("two"), ip_two)]);
            let mut net = Networker::run(dns, sf_one).await;
            let dns = CachingDNS::with(vec![(String::from("one"), ip_one)]);
            let mut net2 = Networker::run(dns, sf_two).await;

            let to = NetworkContact::new("two".to_owned());
            let msg = NetworkMessage::new_accept(to.clone(), NodeID::new());
            net.send_message(msg.clone(), &to).await.unwrap();
            let mut rx = net2.recieve_channel().await.unwrap();

            let m = timeout(Duration::from_micros(TEST_TIMEOUT_MICROS), rx.recv())
                .await
                .unwrap()
                .unwrap();
            match m {
                NetworkResponse::ReadMessage(m, _from) => {
                    assert_eq!(msg, m);
                }
            }
        });
    }

    #[test]
    fn networkers_send_message_over_mock_sockets_bad_dns_on_sender() {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let ip_one = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8000);
            let ip_two = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 2)), 8001);
            let (sf_one, sf_two) = MockSocketFactoryTwins::new(ip_one, ip_two);
            let dns = CachingDNS::with(vec![]);
            let mut net = Networker::run(dns, sf_one).await;
            let dns = CachingDNS::with(vec![(String::from("one"), ip_one)]);
            let _net2 = Networker::run(dns, sf_two).await;

            let to = NetworkContact::new("two".to_owned());
            let msg = NetworkMessage::new_accept(to.clone(), NodeID::new());
            match net.send_message(msg.clone(), &to).await {
                Ok(_) => {
                    panic!();
                }
                Err(e) => {
                    if let ErrorKind::DNSError = e.kind {
                    } else {
                        panic!();
                    }
                }
            }
        });
    }
}
