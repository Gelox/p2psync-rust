use difference::{diff, Difference};
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

///A change to a file
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct FileChange {
    pub file_name: PathBuf,
    pub buf: Vec<FileDifference>,
}

///The differences of a file
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum FileDifference {
    Same(String),
    Add(String),
    Rem(String),
}

impl From<Difference> for FileDifference {
    fn from(diff: Difference) -> FileDifference {
        match diff {
            Difference::Same(s) => FileDifference::Same(s),
            Difference::Add(s) => FileDifference::Add(s),
            Difference::Rem(s) => FileDifference::Rem(s),
        }
    }
}

impl FileChange {
    pub fn new(old_content: &str, new_content: &str, file_path: &Path) -> FileChange {
        let (_, changeset) = diff(old_content, new_content, "\n");
        let mut buf: Vec<FileDifference> = Vec::with_capacity(changeset.len());
        for diff in changeset {
            buf.push(FileDifference::from(diff));
        }
        FileChange {
            file_name: PathBuf::from(file_path),
            buf,
        }
    }
    /*
    pub fn new_from_buf(buf: Vec<FileDifference>, file_name: PathBuf) -> FileChange {
        FileChange { file_name, buf }
    }
    */
}
