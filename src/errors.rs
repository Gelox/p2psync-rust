use crate::network_types::{NetworkCommand, NetworkResponse};
use crate::types::FileChange;
use std::path::PathBuf;
use tokio::sync::mpsc::error::SendError;

///Result type with associated error
pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug, Clone, Copy)]
pub enum ErrorKind {
    MonitorFileError,
    NetworkError,
    SerializationError,
    DNSError,
    NodeError,
}

#[derive(Debug)]
pub struct Error {
    pub kind: ErrorKind,
    pub msg: String,
    pub error: Box<dyn std::error::Error + Send + Sync>,
}

impl Error {
    pub fn new<E>(kind: ErrorKind, error: E) -> Self
    where
        E: Into<Box<dyn std::error::Error + Send + Sync>>,
    {
        let error = error.into();
        Error {
            kind,
            error,
            msg: String::new(),
        }
    }

    pub fn node_error<E>(error: E) -> Self
    where
        E: Into<Box<dyn std::error::Error + Send + Sync>>,
    {
        let error = error.into();
        Error {
            kind: ErrorKind::NodeError,
            error,
            msg: String::new(),
        }
    }

    pub fn with_msg<E>(kind: ErrorKind, error: E, msg: String) -> Self
    where
        E: Into<Box<dyn std::error::Error + Send + Sync>>,
    {
        let error = error.into();
        Error { kind, error, msg }
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(error: serde_yaml::Error) -> Self {
        Error::new(ErrorKind::SerializationError, error)
    }
}

impl From<SendError<PathBuf>> for Error {
    fn from(error: SendError<PathBuf>) -> Self {
        Error::new(ErrorKind::MonitorFileError, error)
    }
}

impl From<tokio::sync::mpsc::error::SendError<FileChange>> for Error {
    fn from(error: tokio::sync::mpsc::error::SendError<FileChange>) -> Self {
        Error::new(ErrorKind::MonitorFileError, error)
    }
}

impl From<notify::Error> for Error {
    fn from(error: notify::Error) -> Self {
        Error::new(ErrorKind::MonitorFileError, error)
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Error::new(ErrorKind::MonitorFileError, error)
    }
}

impl From<tokio::sync::mpsc::error::SendError<NetworkCommand>> for Error {
    fn from(error: tokio::sync::mpsc::error::SendError<NetworkCommand>) -> Self {
        Error::new(ErrorKind::NetworkError, error)
    }
}

impl From<tokio::sync::mpsc::error::SendError<NetworkResponse>> for Error {
    fn from(error: tokio::sync::mpsc::error::SendError<NetworkResponse>) -> Self {
        Error::new(ErrorKind::NetworkError, error)
    }
}
