use async_trait::async_trait;
use notify::event::{Event, EventKind, ModifyKind, RenameMode};
use notify::{RecommendedWatcher, RecursiveMode, Watcher};
use std::collections::{hash_map::Entry::Occupied, HashMap};
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use tokio::sync::mpsc::{channel, Receiver, Sender};

use crate::errors::{Error, Result};
use crate::types::FileChange;

use crate::logger::{log, log_err};

const CHANNEL_SIZE: usize = 100;

///Monitors files if one of the files is changed then a `FileChange` instance is sent
#[async_trait]
pub trait IFileMonitor: Sized + Send + Sync {
    ///Start a new thread that monitors all files sent to it. Changes are returned through channel
    async fn create() -> (Self, Receiver<FileChange>);
    ///Monitors given files
    async fn monitor_file<P: AsRef<Path> + std::marker::Send>(&mut self, path: P) -> Result<()>;
}

///Implementation of the IFileMonitor trait
#[derive(Debug)]
pub struct FileMonitor {
    new_file_tx: Sender<PathBuf>,
}

#[async_trait]
impl IFileMonitor for FileMonitor {
    async fn create() -> (Self, Receiver<FileChange>) {
        log(format!("FileMonitor::create: called"));
        let (change_tx, change_rx) = channel(CHANNEL_SIZE);
        let (new_file_tx, new_file_rx) = channel(CHANNEL_SIZE);
        tokio::spawn(listen_to_changes(change_tx, new_file_rx));

        (FileMonitor { new_file_tx }, change_rx)
    }

    async fn monitor_file<P: AsRef<Path> + std::marker::Send>(&mut self, path: P) -> Result<()> {
        log(format!("FileMonitor::monitor_file: called"));
        let mut p = PathBuf::new();
        p.push(path);
        self.new_file_tx.send(p).await?;
        Ok(())
    }
}

//We use this function since we need to transform our sync channel used by Notify into an async
//channel that can be used in the rest of the monitor.
async fn transform_sync_to_async_channel(
    sync_rx: std::sync::mpsc::Receiver<Event>,
) -> Receiver<Event> {
    log(format!(
        "FileMonitor::transform_sync_to_async_channel: called"
    ));
    let (tx, rx) = channel(CHANNEL_SIZE);
    tokio::task::spawn_blocking(move || {
        log("FileMonitor::transform_sync_to_async_channel: started listening thread");
        for event in sync_rx {
            let mut t = tx.clone();
            tokio::spawn(async move {
                if let Err(e) = t.send(event).await {
                    println!(
                        "transform_sync_to_async_channel: could not send on async channel: {:?}",
                        e
                    );
                }
            });
        }
    });
    rx
}

const FILE_CREATION_SLEEP_DURATION_MILIS: u64 = 20;

async fn listen_to_changes(
    mut file_change_tx: Sender<FileChange>,
    mut monitor_file_rx: Receiver<PathBuf>,
) -> Result<()> {
    log(format!("FileMonitor::listen_to_changes: called"));
    let mut file_content: HashMap<PathBuf, String> = HashMap::new();

    let (tx, rx) = std::sync::mpsc::channel();
    let mut rx = transform_sync_to_async_channel(rx).await;
    let mut watcher: RecommendedWatcher =
        Watcher::new_immediate(move |res: notify::Result<Event>| match res {
            Ok(event) => {
                if let Err(e) = tx.send(event) {
                    println!("Send error: {:?}", e);
                }
            }
            Err(e) => println!("Watch error: {:?}", e),
        })?;

    loop {
        log("FileMonitor::listen_to_changes: looping again...");
        tokio::select! {
            Some(event) = rx.recv() => {
                log(format!("FileMonitor::listen_to_changes: got event..."));
                match event.kind {
                    EventKind::Modify(ModifyKind::Name(RenameMode::From)) => {
                        for path in event.paths.iter() {
                            if let Occupied(mut entry) = file_content.entry(path.to_path_buf()) {
                                log("FileMonitor::listen_to_changes: opening file for comparison...");
                                let mut file = match File::open(path) {
                                    Ok(file) => file,
                                    Err(e) => {
                                        if let std::io::ErrorKind::NotFound = e.kind() {
                                            //We need to sleep sometimes in the case where a file is
                                            //being replaced (which happens when editors like vim write
                                            //to files) it could be that the new file has not yet been
                                            //written
                                            //TODO we need to figure out how to deal with errors here.
                                            //What if the users saves the same file many many many
                                            //times in a short amount of time.
                                            tokio::time::delay_for(std::time::Duration::from_millis(FILE_CREATION_SLEEP_DURATION_MILIS)).await;
                                            match File::open(path) {
                                                Ok(file) => file,
                                                Err(e) => {
                                                    log_err(format!("FileMonitor::listen_to_changes could not open file: {:?}. Error: {}", path, e));
                                                    break;
                                                }
                                            }
                                        } else {
                                            log_err(format!("FileMonitor::listen_to_changes could not open file: {:?}. Error: {}", path, e));
                                            break;
                                        }
                                    }
                                };
                                let mut new_content = String::new();
                                match file.read_to_string(&mut new_content) {
                                    Ok(_) => (),
                                    Err(e) => {
                                        log_err(format!("FileMonitor::listen_to_changes could not read file. Error: {}", e));
                                        break;
                                    }
                                };

                                let old_content = entry.get();
                                let file_change = FileChange::new(&old_content, &new_content, &path.as_path());
                                entry.insert(new_content);
                                match file_change_tx.send(file_change).await {
                                    Ok(_) => (),
                                    Err(e) => {
                                        log_err(format!("FileMonitor::listen_to_changes could not send filechange. Error: {}", e));
                                    }
                                }
                            };
                        }
                    },
                    _ => log(format!("FileMonitor::got file event: {:?}", event.kind)),
                };
            },
            Some(file_name) = monitor_file_rx.recv() => {
                log(format!("FileMonitor::listen_to_changes: got file_name...{:?}", file_name));
                let mut file = match File::open(&file_name) {
                    Ok(file) => file,
                    Err(e) => {
                        log_err(format!("FileMonitor::listen_to_changes: could not open file: {:?}\n{:?}", file_name, e));
                        return Err(Error::from(e));
                    }
                };
                let mut content = String::new();
                file.read_to_string(&mut content)?;

                let parent = match file_name.parent() {
                    Some(parent) => parent,
                    None => &file_name,
                };
                if let Err(e) = watcher.watch(&parent, RecursiveMode::NonRecursive) {
                    log_err(format!("FileMonitor::listen_to_changes: could not watch file: {:?}\n{:?}", parent, e));
                }
                log(format!("FileMonitor::listen_to_changes: watched {:?}", file_name));
                file_content.insert(file_name, content);
            },
            else => {
                log(format!("FileMonitor::listen_to_changes: breaking out of loop..."));
                break
            },
        }
    }
    log("FileMonitor::listen_to_changes: returning OK...");
    Ok(())
}
