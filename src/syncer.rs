use crate::errors::{Error, ErrorKind, Result};
use crate::file_monitor::IFileMonitor;
use crate::network_types::{NetworkContact, NetworkMessage, NetworkMessagePeer, NetworkResponse};
use crate::networker::INetworker;
use crate::node_config::{NodeConfig, NodeID};
use crate::nodes::{Node, NodeView};
use crate::types::FileChange;
use tokio::select;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::oneshot;

type CommandPackage = (SyncerCommands, oneshot::Sender<Result<SyncerResults>>);
#[derive(Debug)]
enum SyncerResults {
    Empty,
    CurrentNodes(Vec<NodeView>),
    PendingInvites(Vec<(NetworkContact, NodeConfig)>),
}
#[derive(Debug)]
enum SyncerCommands {
    NewNode(NodeConfig),
    DeleteNode(NodeID),
    SendInvites(NodeID, Vec<NetworkContact>),
    AcceptInvite(NodeID),
    GetPendingInvites,
    GetCurrentNodes,
}
///Driver for the networker and file monitor and connection between these and the nodes
pub struct Syncer {
    command_tx: Sender<CommandPackage>,
}

const CHANNEL_SIZE: usize = 100;

impl Syncer {
    /// Accepts the invite of the given nodeID
    pub async fn accept_invite(&mut self, id: NodeID) -> Result<()> {
        let (tx, rx) = oneshot::channel();
        self.command_tx
            .send((SyncerCommands::AcceptInvite(id), tx))
            .await
            .expect("Syncer::accept_invite could not send node");
        match rx
            .await
            .expect("Syncer::accept_invite could not recieve on oneshot")
        {
            Ok(sync_result) => {
                if let SyncerResults::Empty = sync_result {
                    Ok(())
                } else {
                    panic!("Expected empty result from syncer");
                }
            }
            Err(e) => Err(e),
        }
    }

    ///Adds a new node with the given node config
    pub async fn new_node(&mut self, config: NodeConfig) -> Result<()> {
        let (tx, rx) = oneshot::channel();
        self.command_tx
            .send((SyncerCommands::NewNode(config), tx))
            .await
            .expect("Syncer::new_node could not send node");
        match rx
            .await
            .expect("Syncer::new_node could not recieve on oneshot")
        {
            Ok(sync_result) => {
                if let SyncerResults::Empty = sync_result {
                    Ok(())
                } else {
                    panic!("Expected empty result from syncer");
                }
            }
            Err(e) => Err(e),
        }
    }

    /// Get the pending invites that are avaliable
    pub async fn get_pending_invites(&mut self) -> Result<Vec<(NetworkContact, NodeConfig)>> {
        let (tx, rx) = oneshot::channel();
        self.command_tx
            .send((SyncerCommands::GetPendingInvites, tx))
            .await
            .expect("Syncer::get_pending_invites could not send command");
        match rx
            .await
            .expect("Syncer::get_pending_invites could not recieve on oneshot")
        {
            Ok(SyncerResults::PendingInvites(r)) => Ok(r),
            Ok(_) => {
                panic!("Expected pending invites result from syncer");
            }
            Err(e) => Err(e),
        }
    }

    ///Send invites for the provided node to the provided contacts
    pub async fn send_invites(&mut self, id: NodeID, contacts: Vec<NetworkContact>) -> Result<()> {
        let (tx, rx) = oneshot::channel();
        self.command_tx
            .send((SyncerCommands::SendInvites(id, contacts), tx))
            .await
            .expect("Syncer::delete_node could not send command");
        match rx
            .await
            .expect("Syncer::delete_node could not recieve on oneshot")
        {
            Ok(SyncerResults::Empty) => Ok(()),
            Ok(_) => {
                panic!("Expected empty result from syncer");
            }
            Err(e) => Err(e),
        }
    }

    ///Deletes a node with the given ID
    pub async fn delete_node(&mut self, id: NodeID) -> Result<()> {
        let (tx, rx) = oneshot::channel();
        self.command_tx
            .send((SyncerCommands::DeleteNode(id), tx))
            .await
            .expect("Syncer::delete_node could not send command");
        match rx
            .await
            .expect("Syncer::delete_node could not recieve on oneshot")
        {
            Ok(SyncerResults::Empty) => Ok(()),
            Ok(_) => {
                panic!("Expected empty result from syncer");
            }
            Err(e) => Err(e),
        }
    }

    ///Get the view for all currently avaliable nodes
    pub async fn get_current_nodes(&mut self) -> Result<Vec<NodeView>> {
        let (tx, rx) = oneshot::channel();
        self.command_tx
            .send((SyncerCommands::GetCurrentNodes, tx))
            .await
            .expect("Syncer::new_node could not send node");
        let res = rx
            .await
            .expect("Syncer::new_node could not recieve on oneshot");
        match res {
            Ok(SyncerResults::CurrentNodes(r)) => Ok(r),
            Ok(_) => panic!("Expected node view result from syncer"),
            Err(e) => Err(e),
        }
    }

    ///Spawns a new thread that drives the networker, file monitor and connects these to the nodes.
    #[allow(unreachable_code)]
    pub async fn new(
        mut net: Box<impl INetworker + 'static>,
        mut fm: Box<impl IFileMonitor + 'static>,
        mut fc_rx: Receiver<FileChange>,
        start_nodes: Vec<NodeConfig>,
    ) -> Result<Self> {
        let (command_tx, mut command_rx): (Sender<CommandPackage>, Receiver<CommandPackage>) =
            channel(CHANNEL_SIZE);
        tokio::spawn(async move {
            let mut nodes: Vec<Node> = vec![];
            let mut pending_invites: Vec<(NetworkContact, NodeConfig)> = vec![];

            for nodeconfig in start_nodes {
                nodes.push(Node::new(nodeconfig, &mut *fm).await?);
            }
            let mut net_rx = net.recieve_channel().await.unwrap();

            loop {
                select! {
                    Some(net_response) = net_rx.recv() => {
                        match net_response {
                            NetworkResponse::ReadMessage(msg, sending_contact) => {
                                match msg {
                                    NetworkMessage::NodeMessage(msg, to) => {
                                        for node in &mut nodes {
                                            if to == node.id {
                                                node.recieve_message(msg).await?;
                                                break;
                                            }
                                        }
                                    }
                                    NetworkMessage::PeerMessage(msg) => {
                                        match msg {
                                            NetworkMessagePeer::NodeInvite(node_config) => {
                                                pending_invites.push((sending_contact, node_config));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Some(fc) = fc_rx.recv() => {
                        for node in &mut nodes {
                            node.new_file_change(&fc, &mut *net).await?;
                        }
                    }
                    Some((command, oneshot_sender)) = command_rx.recv() => {
                        match command {
                            SyncerCommands::NewNode(new_node_config) => {
                                match Node::new(new_node_config, &mut *fm).await {
                                    Ok(new_node) => {
                                        nodes.push(new_node);
                                        oneshot_sender.send(Ok(SyncerResults::Empty))
                                            .expect("sync::new Could not send OK from new node call");
                                    }
                                    Err(e) => {
                                        oneshot_sender.send(Err(e))
                                            .expect("sync::new Could not send error from new node call");
                                    }
                                }
                            },
                            SyncerCommands::DeleteNode(id) => {
                                //TODO need to save the nodes that are not deleted from all the
                                //contacts in order to remind the contacts that they should delete
                                //the node
                                //TODO should make sure the node is allowed to be deleted
                                let mut node_to_delete = None;
                                for node in &mut nodes {
                                    if node.id == id {
                                        let _contacts_not_deleting_node = node.prepare_to_delete(&mut *net).await?;
                                        node_to_delete = Some(node.id);
                                    }
                                }
                                if let Some(to_delete) = node_to_delete {
                                    let index = nodes.iter().position(|n| n.id == to_delete).unwrap();
                                    nodes.remove(index);
                                    oneshot_sender.send(Ok(SyncerResults::Empty))
                                            .expect("sync::new Could not send OK from delete node call");
                                } else {
                                    oneshot_sender.send(Err(Error::node_error("Could not delete node")))
                                            .expect("sync::new Could not send error from delete node call");
                                }
                            },
                            SyncerCommands::SendInvites(id, contacts) => {
                                let mut send_back_val = None;
                                for node in &mut nodes {
                                    if node.id == id {
                                        let ret = node.invite_contacts(contacts, &mut *net).await;
                                        if let Err(uncontacted) = ret {
                                            send_back_val = Some(Err(Error::new(
                                                        ErrorKind::NetworkError,
                                                        format!("Could not send invite to the following contacts {:?}",
                                                                uncontacted))));

                                        } else {
                                            send_back_val = Some(Ok(SyncerResults::Empty));
                                        }
                                        break;
                                    }
                                }
                                match send_back_val {
                                    None => {
                                        oneshot_sender.send(Err(Error::node_error("Node not found")))
                                            .expect("sync::new Could not send Error from invite contact call");
                                    },
                                    Some(Ok(r)) => {
                                        oneshot_sender.send(Ok(r))
                                            .expect("sync::new Could not send Ok from invite contact call");
                                    },
                                    Some(Err(r)) => {
                                        oneshot_sender.send(Err(r))
                                            .expect("sync::new Could not send Error from invite contact call");
                                    },
                                }
                            },
                            SyncerCommands::GetCurrentNodes => {
                                let mut v = vec![];
                                for node in &nodes {
                                    v.push(node.get_view().await);
                                }
                                oneshot_sender.send(Ok(SyncerResults::CurrentNodes(v)))
                                    .expect("sync::new Could not send result from get_current_nodes call");

                            },
                            SyncerCommands::AcceptInvite(id) => {
                                //TODO we should probably send network messages that we accepted
                                //somewhere here, for example inside the node
                                let idx = match pending_invites.iter().position(|(_contact, config)|{
                                    config.node_id == id
                                }) {
                                    Some(idx) => idx,
                                    None => {
                                        oneshot_sender.send(Err(Error::node_error("Could not find provided node")))
                                            .expect("sync::new Could not send result from accept_invite call");
                                        break;
                                    },
                                };
                                let (_, config) = pending_invites.remove(idx);
                                let node = match Node::new(config, &mut *fm).await {
                                    Ok(node) => node,
                                    Err(e) => {
                                        oneshot_sender.send(Err(e))
                                            .expect("sync::new Could not send result from accept_invite call");
                                        break;
                                    },
                                };
                                nodes.push(node);
                                oneshot_sender.send(Ok(SyncerResults::Empty))
                                    .expect("sync::new Could not send result from accept_invite call");
                            },
                            SyncerCommands::GetPendingInvites => {
                                oneshot_sender.send(Ok(SyncerResults::PendingInvites(pending_invites.clone())))
                                    .expect("sync::new Could not send result from get_pending_invites call");
                            }
                        };
                    }
                }
            }
            Ok::<(), Error>(())
        });
        Ok(Syncer { command_tx })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::errors::*;
    use crate::mocks::*;
    use crate::network_types::*;
    use crate::node_config::*;
    use crate::tests::*;
    use crate::types::*;
    use std::path::PathBuf;
    use std::time::Duration;
    use tokio::runtime::*;
    use tokio::time::timeout;
    const TEST_TIMEOUT_MICROS: u64 = 1000;

    async fn setup_mocks(
        configs: Vec<NodeConfig>,
    ) -> (
        Receiver<(NetworkContact, NetworkMessage)>,
        Sender<FileChange>,
        Syncer,
    ) {
        let mut networker = Box::new(MockNetworker::new());
        let messages_sent = networker.sent_messages().unwrap();
        let (mut fm, fc_rx) = MockFileMonitor::create().await;
        let fc_sender = fm.get_fc_channel().unwrap();
        let fm = Box::new(fm);
        let syncer = Syncer::new(networker, fm, fc_rx, configs).await.unwrap();
        (messages_sent, fc_sender, syncer)
    }

    async fn setup_mocks_with_outside_channel(
        configs: Vec<NodeConfig>,
    ) -> (
        Receiver<(NetworkContact, NetworkMessage)>,
        Sender<NetworkResponse>,
        Sender<FileChange>,
        Syncer,
    ) {
        let mut networker = Box::new(MockNetworker::new());
        let messages_sent = networker.sent_messages().unwrap();
        let outside_sender = networker.mocked_outside_channel();
        let (mut fm, fc_rx) = MockFileMonitor::create().await;
        let fc_sender = fm.get_fc_channel().unwrap();
        let fm = Box::new(fm);
        let syncer = Syncer::new(networker, fm, fc_rx, configs).await.unwrap();
        (messages_sent, outside_sender, fc_sender, syncer)
    }

    #[test]
    fn delete_node_expect_failure() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                NodeID::new(),
            );
            let conf_2 = NodeConfig::new(
                vec![std::path::PathBuf::from("Other_file.txt")],
                vec![
                    NetworkContact::new("my_friend".to_string()),
                    NetworkContact::new("my_other_friend".to_string()),
                ],
                NodeID::new(),
            );
            let (_sent_messages, _, mut syncer) = setup_mocks(vec![config.clone()]).await;
            let (mut fm, _) = MockFileMonitor::create().await;

            syncer.new_node(conf_2.clone()).await.unwrap();
            if let Err(e) = syncer.delete_node(NodeID::new()).await {
                if let ErrorKind::NodeError = e.kind {
                } else {
                    panic!();
                }
            } else {
                panic!();
            }

            let current_nodes = syncer.get_current_nodes().await.unwrap();
            assert_eq!(
                current_nodes,
                vec![
                    Node::new(config.clone(), &mut fm)
                        .await
                        .unwrap()
                        .get_view()
                        .await,
                    Node::new(conf_2.clone(), &mut fm)
                        .await
                        .unwrap()
                        .get_view()
                        .await
                ]
            );
        });
    }

    #[test]
    fn delete_node_expect_node_deleted() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                NodeID::new(),
            );
            let conf_2 = NodeConfig::new(
                vec![std::path::PathBuf::from("Other_file.txt")],
                vec![
                    NetworkContact::new("my_friend".to_string()),
                    NetworkContact::new("my_other_friend".to_string()),
                ],
                NodeID::new(),
            );
            let (_sent_messages, _, mut syncer) = setup_mocks(vec![config.clone()]).await;
            let (mut fm, _) = MockFileMonitor::create().await;

            syncer.new_node(conf_2.clone()).await.unwrap();
            let current_nodes = syncer.get_current_nodes().await.unwrap();
            assert_eq!(
                current_nodes,
                vec![
                    Node::new(config.clone(), &mut fm)
                        .await
                        .unwrap()
                        .get_view()
                        .await,
                    Node::new(conf_2.clone(), &mut fm)
                        .await
                        .unwrap()
                        .get_view()
                        .await
                ]
            );
            syncer.delete_node(config.node_id).await.unwrap();

            let current_nodes = syncer.get_current_nodes().await.unwrap();
            assert_eq!(
                current_nodes,
                vec![Node::new(conf_2, &mut fm).await.unwrap().get_view().await]
            );
        });
    }

    #[test]
    fn get_syncer_current_nodes() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                NodeID::new(),
            );
            let conf_2 = NodeConfig::new(
                vec![std::path::PathBuf::from("Other_file.txt")],
                vec![
                    NetworkContact::new("my_friend".to_string()),
                    NetworkContact::new("my_other_friend".to_string()),
                ],
                NodeID::new(),
            );
            let (_, _, mut syncer) = setup_mocks(vec![config.clone()]).await;
            let (mut fm, _) = MockFileMonitor::create().await;

            syncer.new_node(conf_2.clone()).await.unwrap();

            let current_nodes = syncer.get_current_nodes().await.unwrap();
            assert_eq!(
                current_nodes,
                vec![
                    Node::new(config, &mut fm).await.unwrap().get_view().await,
                    Node::new(conf_2, &mut fm).await.unwrap().get_view().await
                ]
            );
        });
    }

    #[test]
    fn mock_file_change_sends_mock_network_message() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                NodeID::new(),
            );
            let (mut sent_msg, mut fc_tx, _syncer) = setup_mocks(vec![config]).await;

            let fc = FileChange::new("Old content", "new content", &PathBuf::from("TestFile.txt"));
            fc_tx.send(fc.clone()).await.unwrap();
            let msg = timeout(Duration::from_micros(TEST_TIMEOUT_MICROS), sent_msg.recv())
                .await
                .unwrap()
                .unwrap();
            let (_contact, msg) = msg;
            if let NetworkMessage::NodeMessage(NetworkMessageNode::FileChange(sent_fc), _id) = msg {
                assert!(fc_cmp(&fc, &sent_fc));
            } else {
                panic!();
            }
        });
    }

    #[test]
    fn unregistered_fc_does_not_cause_network_message() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let fc = FileChange::new("Old content", "new content", &PathBuf::from("TestFile.txt"));
            let mut networker = Box::new(MockNetworker::new());
            let sent_messages = &mut networker.sent_messages().unwrap();
            let (mut fm, fc_rx) = MockFileMonitor::create().await;
            let mut fc_sender = fm.get_fc_channel().unwrap();
            let fm = Box::new(fm);
            let _syncer = Syncer::new(networker, fm, fc_rx, vec![]).await.unwrap();

            fc_sender.send(fc.clone()).await.unwrap();
            if let Ok(_) = timeout(
                Duration::from_micros(TEST_TIMEOUT_MICROS),
                sent_messages.recv(),
            )
            .await
            {
                panic!("Should have timed out");
            }
        });
    }

    #[test]
    fn adding_new_node() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let fc = FileChange::new("Old content", "new content", &PathBuf::from("TestFile.txt"));
            let mut networker = Box::new(MockNetworker::new());
            let sent_messages = &mut networker.sent_messages().unwrap();
            let (mut fm, fc_rx) = MockFileMonitor::create().await;
            let mut fc_sender = fm.get_fc_channel().unwrap();
            let fm = Box::new(fm);
            let mut syncer = Syncer::new(networker, fm, fc_rx, vec![]).await.unwrap();

            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                NodeID::new(),
            );
            syncer.new_node(config).await.unwrap();
            fc_sender.send(fc.clone()).await.unwrap();
            let msg = timeout(
                Duration::from_micros(TEST_TIMEOUT_MICROS),
                sent_messages.recv(),
            )
            .await
            .unwrap()
            .unwrap();
            let (_contact, msg) = msg;

            if let NetworkMessage::NodeMessage(NetworkMessageNode::FileChange(sent_fc), _id) = msg {
                assert!(fc_cmp(&fc, &sent_fc));
            } else {
                panic!();
            }
        });
    }

    #[test]
    fn send_invite_expect_success() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let id = NodeID::new();
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                id,
            );
            let (mut messages_sent, _, mut syncer) = setup_mocks(vec![config.clone()]).await;
            let contact = NetworkContact::new("an_uninvited_friend");
            syncer
                .send_invites(id, vec![contact.clone()])
                .await
                .unwrap();

            let msg = timeout(
                Duration::from_micros(TEST_TIMEOUT_MICROS),
                messages_sent.recv(),
            )
            .await
            .unwrap()
            .unwrap();

            if let (
                recv_contact,
                NetworkMessage::PeerMessage(NetworkMessagePeer::NodeInvite(recv_config)),
            ) = msg
            {
                assert_eq!(config, recv_config);
                assert_eq!(contact, recv_contact);
            } else {
                panic!();
            }
        });
    }

    #[test]
    fn get_pending_invites() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                NodeID::new(),
            );
            let (_messages_sent, mut outside_sender, _, mut syncer) =
                setup_mocks_with_outside_channel(vec![]).await;
            let from = NetworkContact::new("outside_sender");
            let invite = NetworkResponse::ReadMessage(
                NetworkMessage::new_invite(config.clone()),
                from.clone(),
            );

            outside_sender.send(invite).await.unwrap();
            std::thread::sleep(std::time::Duration::from_micros(TEST_TIMEOUT_MICROS));
            let pending_invites = syncer.get_pending_invites().await.unwrap();

            assert_eq!(1, pending_invites.len());
            for (got_from, got_config) in pending_invites {
                assert_eq!(got_from, from);
                assert_eq!(got_config, config);
            }
        });
    }

    #[test]
    fn accept_invite() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let id = NodeID::new();
            let config = NodeConfig::new(
                vec![std::path::PathBuf::from("TestFile.txt")],
                vec![NetworkContact::new("my_friend".to_string())],
                id.clone(),
            );
            let (_messages_sent, mut outside_sender, _, mut syncer) =
                setup_mocks_with_outside_channel(vec![]).await;
            let from = NetworkContact::new("outside_sender");
            let invite = NetworkResponse::ReadMessage(
                NetworkMessage::new_invite(config.clone()),
                from.clone(),
            );

            let cur = syncer.get_current_nodes().await.unwrap();
            assert_eq!(cur.len(), 0);
            outside_sender.send(invite).await.unwrap();
            std::thread::sleep(std::time::Duration::from_micros(TEST_TIMEOUT_MICROS));
            syncer.accept_invite(id).await.unwrap();

            let cur = syncer.get_current_nodes().await.unwrap();
            assert_eq!(cur.len(), 1);
        });
    }
}
