pub fn log<S: AsRef<str>>(msg: S) {
    let _msg = msg.as_ref();
    //println!("{}", _msg);
}

pub fn log_err<S: AsRef<str>>(msg: S) {
    let _msg = msg.as_ref();
    println!("{}", _msg);
}

pub fn log_important<S: AsRef<str>>(msg: S) {
    let _msg = msg.as_ref();
    println!("{}", _msg);
}
