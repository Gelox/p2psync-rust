use crate::errors::{Error, ErrorKind, Result};
use crate::network_types::NetworkContact;
use serde::{Deserialize, Serialize};
use std::io::Read;
use std::io::Write;
use std::path::{Path, PathBuf};

///The identifier for a node
#[derive(Debug, PartialEq, Eq, Clone, Copy, Serialize, Deserialize)]
pub struct NodeID {
    pub id: u64,
}

impl NodeID {
    pub fn new() -> Self {
        NodeID {
            id: rand::random::<u64>(),
        }
    }
}

///A configuration for a node that can be read from a file
#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct NodeConfig {
    pub watched_files: Vec<PathBuf>,
    pub contacts: Vec<NetworkContact>,
    pub node_id: NodeID,
}

////The name of the node config files
//FIXME this one should be rethought or removed
//pub const CONFIG_NODE_NAME: &str = ".node_config.yml";
///The directory containing all of the node configs
const NODE_CONFIG_DIR: &str = "./NODE_CONFIGS/";

impl NodeConfig {
    ///Reads and creates a `NodeConfig` from a file
    pub async fn read_from_file<P: AsRef<Path>>(config_file: P) -> Result<Self> {
        let mut file = match std::fs::File::open(config_file) {
            Ok(file) => file,
            Err(e) => return Err(Error::new(ErrorKind::SerializationError, e)),
        };
        let mut buf = String::new();
        file.read_to_string(&mut buf)?;
        let config: NodeConfig = serde_yaml::from_str(&buf)?;
        Ok(config)
    }

    ///Creates a NodeConfig from its compontents, generates a new ID
    pub async fn spawn(watched_files: Vec<PathBuf>, contacts: Vec<NetworkContact>) -> Result<Self> {
        let mut config_path: PathBuf = PathBuf::from(NODE_CONFIG_DIR);
        let node_id = NodeID::new();
        config_path.push(node_id.id.to_string());
        let mut file = std::fs::File::create(&config_path)?;
        let node_conf = NodeConfig {
            watched_files,
            contacts,
            node_id,
        };
        let serialized: String = serde_yaml::to_string(&node_conf)?;
        file.write_all(serialized.as_bytes())?;
        Ok(node_conf)
    }

    pub fn new(
        watched_files: Vec<PathBuf>,
        contacts: Vec<NetworkContact>,
        node_id: NodeID,
    ) -> Self {
        NodeConfig {
            watched_files,
            contacts,
            node_id,
        }
    }
}
