use crate::errors::Result;
use crate::logger::log_important;
use crate::types::{FileChange, FileDifference};
use async_trait::async_trait;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

///Writes a file change to the appropriate file
#[async_trait]
pub trait IFileWriter {
    ///Writes the change
    async fn write_file_change(&self, file_change: FileChange) -> Result<()>;
}

///Implementation of the `IFileWriter` trait
pub struct FileWriter {}

impl FileWriter {
    pub async fn new() -> Self {
        FileWriter {}
    }
}

#[async_trait]
impl IFileWriter for FileWriter {
    async fn write_file_change(&self, file_change: FileChange) -> Result<()> {
        log_important(format!("FileWriter::write_file_change: {:?}", file_change));
        let name: PathBuf = file_change.file_name;
        let buf = file_change.buf;
        let mut file = File::create(name)?;
        for fd in buf {
            match fd {
                FileDifference::Same(s) => {
                    file.write_all(s.as_bytes())?;
                }
                FileDifference::Add(s) => {
                    file.write_all(s.as_bytes())?;
                }
                FileDifference::Rem(s) => {
                    file.write_all(s.as_bytes())?;
                }
            }
        }
        Ok(())
    }
}
