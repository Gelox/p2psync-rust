mod errors;
mod file_monitor;
mod file_writer;
mod logger;
mod mocks;
mod network_types;
mod networker;
mod node_config;
mod nodes;
mod syncer;
mod types;
mod ui;

use errors::Result;
use file_monitor::*;
use glob::glob;
use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};
use network_types::NetworkContact;
use networker::{dns::CachingDNS, INetworker, Networker, TcpSocketFactory};
use node_config::NodeConfig;
use std::path::PathBuf;
use syncer::Syncer;
use tokio::sync::mpsc::Receiver;
use types::FileChange;
use ui::{tui, UiAction, View};

static LOGGER: SimpleLogger = SimpleLogger;

pub fn logger_init() -> core::result::Result<(), SetLoggerError> {
    log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Info))
}

#[tokio::main]
async fn main() -> Result<()> {
    logger_init().expect("Could not initialize logger");
    let config = match glob(&format!("./{}", ".node_config.yml"))
        .expect("Failed to read glob pattern")
        .next()
    {
        Some(Ok(path)) => {
            let config_path = std::fs::canonicalize(path).expect("Could not canonicalize path");
            let config = NodeConfig::read_from_file(config_path).await?;
            config
        }
        _ => {
            let watched_files = vec![PathBuf::from("foo.txt")];
            let contacts = vec![NetworkContact::new("my_friend".to_owned())];
            let config = NodeConfig::spawn(watched_files, contacts).await?;
            config
        }
    };

    let (networker, fm, fc_rx) = inject().await;
    let mut syncer = Syncer::new(networker, fm, fc_rx, vec![config]).await?;
    loop {
        let view = View::new(
            syncer.get_current_nodes().await?,
            syncer.get_pending_invites().await?,
        );
        match tui(view).await {
            UiAction::NewNode(config) => {
                syncer.new_node(config).await?;
            }
            UiAction::DeleteNode(node_id) => {
                syncer.delete_node(node_id).await?;
            }
            UiAction::ModifyNode(_node_config) => (),
            UiAction::SendInvites(node_id, contacts) => {
                syncer.send_invites(node_id, contacts).await?;
            }
            UiAction::AcceptInvite(node_id) => {
                syncer.accept_invite(node_id).await?;
            }
            UiAction::RevokeInvites(_node_id, _contacts) => (),
            UiAction::ToggleRunstate => (),
            UiAction::Continue => (),
            UiAction::Exit => return Ok(()),
        };
    }
}

async fn inject() -> (
    Box<impl INetworker>,
    Box<impl IFileMonitor>,
    Receiver<FileChange>,
) {
    let (file_monitor, filechange_rx) = FileMonitor::create().await;
    let file_monitor = Box::new(file_monitor);
    let dns = CachingDNS::with(vec![]);
    let socket_factory = TcpSocketFactory::new();
    (
        Box::new(Networker::run(dns, socket_factory).await),
        file_monitor,
        filechange_rx,
    )
}

struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Info
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::*;
    use tokio::runtime::*;

    pub fn fc_cmp(first: &FileChange, second: &FileChange) -> bool {
        first == second
    }

    #[test]
    fn inject_functions() {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(inject());
    }

    //Test in the case for file that should not be monitored
}
