use crate::errors::Result;
use crate::file_monitor::IFileMonitor;
use crate::file_writer::{FileWriter, IFileWriter};
use crate::logger::log;
use crate::network_types::{NetworkContact, NetworkMessage, NetworkMessageNode};
use crate::networker::INetworker;
use crate::node_config::{NodeConfig, NodeID};
use crate::types::FileChange;
use std::path::PathBuf;

/// Is the external view of the node that can be exported
#[derive(Debug, Eq, PartialEq)]
pub struct NodeView {
    pub id: NodeID,
    pub contacts: Vec<NetworkContact>,
    pub watched_files: Vec<PathBuf>,
}

///Describes a group of files and a group of contacts to be notified about changes to those files.
pub struct Node {
    pub id: NodeID,
    fw: FileWriter,
    contacts: Vec<NetworkContact>,
    watched_files: Vec<PathBuf>,
}

impl Node {
    ///Creates a new node with a provided `IFileMonitor`
    pub async fn new(node_config: NodeConfig, fm: &mut impl IFileMonitor) -> Result<Self> {
        let watched_files = node_config.watched_files;
        let contacts = node_config.contacts;
        for file in &watched_files {
            fm.monitor_file(file).await?;
        }
        Ok(Node {
            fw: FileWriter::new().await,
            id: node_config.node_id,
            contacts,
            watched_files,
        })
    }

    async fn get_config(&self) -> NodeConfig {
        NodeConfig::new(
            self.watched_files.clone(),
            self.contacts.clone(),
            self.id.clone(),
        )
    }

    /// Creates a new NodeView
    pub async fn get_view(&self) -> NodeView {
        NodeView {
            id: self.id.clone(),
            contacts: self.contacts.clone(),
            watched_files: self.watched_files.clone(),
        }
    }

    /// Cleans up the things required to delete the node. Returns a list of all the contacts that
    /// could not recieve the message telling them to delete.
    //TODO: Need to delete the configuration file here or in the syncer
    pub async fn prepare_to_delete(
        &mut self,
        net: &mut impl INetworker,
    ) -> Result<Vec<NetworkContact>> {
        let msg = NetworkMessage::new_delete_node(self.id.clone());
        let mut v = vec![];
        for contact in &self.contacts {
            if let Err(e) = net.send_message(msg.clone(), contact).await {
                //TODO here we should register that someone could not recieve that this node should
                //be deleted. In that case we need to register that they should be reminded of this
                //next time we communicate with them
                log(format!(
                    "Could not send message to contact: {:?}\nError: {:?}",
                    contact, e
                ));
                v.push(contact.clone());
            }
        }
        Ok(v)
    }

    /// Sends an invite to the provided contacts and returns an err with any contacts that could
    /// not be reached
    pub async fn invite_contacts(
        &mut self,
        contacts: Vec<NetworkContact>,
        net: &mut impl INetworker,
    ) -> std::result::Result<(), Vec<NetworkContact>> {
        let node_config = self.get_config().await;
        let net_msg = NetworkMessage::new_invite(node_config);
        let mut ret = vec![];
        for contact in contacts {
            if let Err(_) = net.send_message(net_msg.clone(), &contact).await {
                ret.push(contact);
            }
        }
        if ret.is_empty() {
            Ok(())
        } else {
            Err(ret)
        }
    }

    pub async fn accept_invite(&mut self, net: &mut impl INetworker) -> Result<()> {
        let me_contact = NetworkContact::me();
        let net_msg = NetworkMessage::new_accept(me_contact, self.id);
        for contact in &self.contacts {
            net.send_message(net_msg.clone(), contact).await?;
        }
        Ok(())
    }

    ///Receives message content for the node and decides how to deal with it
    pub async fn recieve_message(&mut self, net_content: NetworkMessageNode) -> Result<()> {
        match net_content {
            NetworkMessageNode::FileChange(fc) => {
                self.fw.write_file_change(fc).await?;
            }
            //TODO
            NetworkMessageNode::AcceptInvite(_contact) => {}
            NetworkMessageNode::DeleteNode => (),
        };
        Ok(())
    }

    ///Notifies the node about a file change to the local files. It is possible that the file
    ///change is not relevant for the node, in that case it is the responsibility of the node to
    ///sort out irrelevant changes.
    pub async fn new_file_change(
        &mut self,
        fc: &FileChange,
        net: &mut impl INetworker,
    ) -> Result<()> {
        for file in &self.watched_files {
            if *file == fc.file_name {
                let net_msg = NetworkMessage::new_file_change(fc.clone(), self.id);
                for contact in &self.contacts {
                    net.send_message(net_msg.clone(), contact).await?;
                }
                break;
            }
        }
        Ok(())
    }
}
