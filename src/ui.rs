use crate::network_types::NetworkContact;
use crate::node_config::NodeConfig;
use crate::node_config::NodeID;
use crate::nodes::NodeView;
use std::io::stdin;
use std::path::PathBuf;
use termion::clear::All;
use termion::input::TermRead;

pub enum UiAction {
    NewNode(NodeConfig),
    DeleteNode(NodeID),
    ModifyNode(NodeConfig),
    AcceptInvite(NodeID),
    SendInvites(NodeID, Vec<NetworkContact>),
    RevokeInvites(NodeID, Vec<NetworkContact>),
    ToggleRunstate,
    Continue,
    Exit,
}

pub struct View {
    nodes: Vec<NodeView>,
    sent_invites: Vec<(NodeID, NetworkContact)>,
    pending_invites: Vec<(NodeID, NetworkContact)>,
}

impl View {
    pub fn new(nodes: Vec<NodeView>, pending_invites: Vec<(NetworkContact, NodeConfig)>) -> Self {
        let pending_invites: Vec<_> = pending_invites
            .iter()
            .map(|(contact, config)| (config.node_id, contact.clone()))
            .collect();
        Self {
            nodes,
            sent_invites: vec![],
            pending_invites,
        }
    }
}

pub async fn tui(view: View) -> UiAction {
    let runstate = "ON";
    let invites = view.pending_invites.len();
    let sent_invites = view.sent_invites.len();
    println!("{}", All);
    println!("Choose an option");
    println!("1. Create new node");
    println!("2. List all nodes");
    println!("3. Delete node");
    println!("4. Modify node");
    println!("5. Toggle run-state (currently: {})", runstate);
    println!("6. Accept invites (currently: {} invites)", invites);
    println!("7. Send invite");
    println!(
        "8. Revoke invites (currently: {} invites sent)",
        sent_invites
    );
    println!("9. List invites");
    println!("0. Exit");
    let input = get_input_line().await;
    if input == "1" {
        return UiAction::NewNode(create_new_node().await);
    } else if input == "2" {
        list_nodes(&view).await;
        return UiAction::Continue;
    } else if input == "3" {
        return UiAction::DeleteNode(delete_node(&view).await);
    } else if input == "4" {
        return UiAction::ModifyNode(modify_node(&view).await);
    } else if input == "5" {
        return UiAction::ToggleRunstate;
    } else if input == "6" {
        return UiAction::AcceptInvite(accept_invite(&view).await);
    } else if input == "7" {
        let (id, contacts) = send_invites(&view).await;
        return UiAction::SendInvites(id, contacts);
    } else if input == "8" {
        let (id, contacts) = revoke_invites(&view).await;
        return UiAction::RevokeInvites(id, contacts);
    } else if input == "9" {
        list_invites(&view).await;
        return UiAction::Continue;
    } else {
        return UiAction::Exit;
    }
}

/// List the invites that you have recieved to nodes
async fn list_my_pending_invites(view: &View) {
    println!("Invites sent to you");
    println!("Node ID : Sent from Contact");
    for (node_id, from) in &view.sent_invites {
        println!("{:?} : {:?}", node_id, from);
    }
}

/// List the invites to nodes you are part of that have been sent to others
async fn list_sent_invites(view: &View) {
    println!("Invites sent to nodes you are a part of");
    println!("Node ID : Contact");
    for (node_id, to) in &view.sent_invites {
        println!("{:?} : {:?}", node_id, to);
    }
}

/// Lists all of invite specific actions
async fn list_invites(view: &View) {
    let pending_sent_invites = view.sent_invites.len();
    let pending_invites_to_you = view.pending_invites.len();
    println!("{}", All);
    println!(
        "Currently you have {} invites, press 1 to see them",
        pending_invites_to_you
    );
    println!(
        "Currently {} invites for nodes you are part of have been sent, press 2 to see them",
        pending_sent_invites
    );
    println!("Press anything else to exit");
    loop {
        let input = get_input_line().await;
        if input == "1" {
            list_my_pending_invites(view).await;
        } else if input == "2" {
            list_sent_invites(view).await;
        } else {
            break;
        }
    }
}

/// Choose a node and the people to revoke invites from
async fn revoke_invites(view: &View) -> (NodeID, Vec<NetworkContact>) {
    list_nodes(view).await;
    println!("Which node do you want to revoke invites from?");
    print!("Node #: ");
    let id = get_node_id(view).await;
    println!("Enter all the names of who you want to revoke invites from, end with an empty line");
    let mut contacts = vec![];
    loop {
        let name = get_input_line().await;
        if name == "" {
            break;
        }
        contacts.push(NetworkContact::new(name));
    }
    (id, contacts)
}

/// Choose a node and the people to invite to that node
async fn send_invites(view: &View) -> (NodeID, Vec<NetworkContact>) {
    list_nodes(view).await;
    println!("Which node do you want to invite people to?");
    print!("Node #: ");
    let id = get_node_id(view).await;
    println!("Enter all the names of who you want to invite, end with an empty line");
    let mut contacts = vec![];
    loop {
        let name = get_input_line().await;
        if name == "" {
            break;
        }
        contacts.push(NetworkContact::new(name));
    }
    (id, contacts)
}

async fn get_node_id_from_invites(view: &View) -> NodeID {
    let nodeid;
    loop {
        let input = get_input_line().await;
        let input = match input.parse::<usize>() {
            Ok(r) => r,
            Err(_) => continue,
        };
        match view.pending_invites.get(input) {
            Some((n, _)) => {
                nodeid = n;
                break;
            }
            None => continue,
        }
    }
    *nodeid
}

/// Choose which invite to accept
async fn accept_invite(view: &View) -> NodeID {
    println!("# : NodeID : Invited by");
    println!("Which node invite do you want to accept?");
    for (i, (node_id, contact)) in view.pending_invites.iter().enumerate() {
        println!("{} : {} : {}", i, node_id.id, contact.username);
    }
    let id = get_node_id_from_invites(view).await;
    id
}

//TODO
async fn modify_node(_view: &View) -> NodeConfig {
    //list_nodes(view).await;
    //println!("Which node would you like to delete?");
    //print!("Node #: ");
    //let id = get_node_id(view).await;
    //id;
    NodeConfig::new(vec![], vec![], NodeID::new())
}

/// List all of the nodes
async fn list_nodes(view: &View) {
    println!("Current nodes:");
    println!("# : ID : First watched files : First contact");
    for (i, node) in view.nodes.iter().enumerate() {
        let first_watched = match node.watched_files.get(0) {
            Some(r) => r.to_str().unwrap(),
            None => "None",
        };
        let first_contact = match node.contacts.get(0) {
            Some(r) => &r.username,
            None => "None",
        };
        println!(
            "{} : {} : {} : {}",
            i, node.id.id, first_watched, first_contact
        );
    }
}

async fn get_node_id(view: &View) -> NodeID {
    let node;
    loop {
        let input = get_input_line().await;
        let input = match input.parse::<usize>() {
            Ok(r) => r,
            Err(_) => continue,
        };
        match view.nodes.get(input) {
            Some(n) => {
                node = n;
                break;
            }
            None => continue,
        }
    }
    node.id
}

/// Choose which node to delete
async fn delete_node(view: &View) -> NodeID {
    list_nodes(view).await;
    println!("Which node would you like to delete?");
    print!("Node #: ");
    let id = get_node_id(view).await;
    id
}

async fn get_input_line() -> String {
    let stdin = stdin();
    let mut stdin = stdin.lock();
    stdin.read_line().unwrap().unwrap()
}

async fn create_new_node() -> NodeConfig {
    println!("Which files should be watched initially?");
    let mut file_input = get_input_line().await;
    let mut watched_files = vec![];
    while file_input != "" {
        watched_files.push(PathBuf::from(file_input));
        file_input = get_input_line().await;
    }

    println!("Which contacts are initially invited?");
    let mut contact_input = get_input_line().await;
    let mut contacts = vec![];
    while contact_input != "" {
        let cont = NetworkContact::new(contact_input);
        contacts.push(cont);
        contact_input = get_input_line().await;
    }

    NodeConfig::new(watched_files, contacts, NodeID::new())
}
