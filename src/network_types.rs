use crate::node_config::{NodeConfig, NodeID};
use crate::types::FileChange;
use serde::{Deserialize, Serialize};

///A peer in the network
#[derive(Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct NetworkContact {
    pub username: String,
}

impl NetworkContact {
    pub fn me() -> Self {
        NetworkContact {
            username: String::from("my_username123"),
        }
    }

    pub fn new<S: ToString>(username: S) -> Self {
        NetworkContact {
            username: username.to_string(),
        }
    }
}

///A network message, this is the struct that is sent between peers
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub enum NetworkMessage {
    NodeMessage(NetworkMessageNode, NodeID),
    PeerMessage(NetworkMessagePeer),
}

///The content of a network message with routing data stripped for general peers
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub enum NetworkMessagePeer {
    NodeInvite(NodeConfig),
}

///The content of a network message with routing data stripped for specific nodes
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub enum NetworkMessageNode {
    DeleteNode,
    FileChange(FileChange),
    AcceptInvite(NetworkContact),
}

impl NetworkMessage {
    pub fn new_file_change(fc: FileChange, node_id: NodeID) -> Self {
        NetworkMessage::NodeMessage(NetworkMessageNode::FileChange(fc), node_id)
    }

    pub fn new_invite(node_config: NodeConfig) -> Self {
        NetworkMessage::PeerMessage(NetworkMessagePeer::NodeInvite(node_config))
    }

    pub fn new_accept(contact: NetworkContact, node_id: NodeID) -> Self {
        NetworkMessage::NodeMessage(NetworkMessageNode::AcceptInvite(contact), node_id)
    }
    pub fn new_delete_node(node_id: NodeID) -> Self {
        NetworkMessage::NodeMessage(NetworkMessageNode::DeleteNode, node_id)
    }
}

//TODO this one should perhaps be removed or at least be made private
///Message that describe the networker ought to do only used for internals and might be removed
///later
#[derive(Debug)]
pub enum NetworkCommand {
    SendMessage(NetworkMessage, NetworkContact),
}

///A message returned by the networker
#[derive(Debug)]
pub enum NetworkResponse {
    //Read a message from a specified contact
    ReadMessage(NetworkMessage, NetworkContact),
}
