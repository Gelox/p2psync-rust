#![allow(dead_code)]
use super::errors::*;
use super::file_monitor::IFileMonitor;
use super::network_types::{NetworkContact, NetworkMessage, NetworkResponse};
use super::networker::INetworker;
use crate::networker::{SocketFactory, SocketListener, SocketStream};
use crate::types::*;
use async_trait::async_trait;
use futures::task::Context;
use futures::task::Poll;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::path::Path;
use std::pin::Pin;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio::net::ToSocketAddrs;
use tokio::stream::Stream;
use tokio::sync::mpsc::{channel, Receiver, Sender};

pub struct MockFileMonitor {
    sender: Option<Sender<FileChange>>,
}

impl MockFileMonitor {
    pub fn get_fc_channel(&mut self) -> Option<Sender<FileChange>> {
        self.sender.take()
    }
}

#[async_trait]
impl IFileMonitor for MockFileMonitor {
    async fn create() -> (Self, Receiver<FileChange>) {
        let (tx, rx) = channel(100);
        (Self { sender: Some(tx) }, rx)
    }

    async fn monitor_file<P: AsRef<Path> + std::marker::Send>(&mut self, _path: P) -> Result<()> {
        Ok(())
    }
}

pub struct MockNetworker {
    sent_messages: Option<Receiver<(NetworkContact, NetworkMessage)>>,
    sent_messages_sender: Sender<(NetworkContact, NetworkMessage)>,
    //Mocking the connection from the outside
    outside_sender: Sender<NetworkResponse>,
    recieved_messages: Option<Receiver<NetworkResponse>>,
}

impl MockNetworker {
    pub fn new() -> Self {
        let (tx, rx) = channel(100);
        let (sent_messages_sender, sent_messages) = channel(100);
        MockNetworker {
            sent_messages: Some(sent_messages),
            sent_messages_sender,
            outside_sender: tx,
            recieved_messages: Some(rx),
        }
    }

    pub fn sent_messages(&mut self) -> Option<Receiver<(NetworkContact, NetworkMessage)>> {
        self.sent_messages.take()
    }

    pub fn mocked_outside_channel(&self) -> Sender<NetworkResponse> {
        self.outside_sender.clone()
    }
}

#[async_trait]
impl INetworker for MockNetworker {
    async fn send_message(&mut self, msg: NetworkMessage, to: &NetworkContact) -> Result<()> {
        self.sent_messages_sender
            .send((to.clone(), msg.clone()))
            .await
            .unwrap();
        Ok(())
    }

    async fn recieve_channel(&mut self) -> Option<Receiver<NetworkResponse>> {
        match self.recieved_messages {
            Some(_) => self.recieved_messages.take(),
            None => None,
        }
    }
}

pub struct MockSocket {
    send: Sender<[u8; 1500]>,
    recv: Receiver<[u8; 1500]>,
}

pub struct MockSocketListener {
    recv: Receiver<MockSocket>,
}

pub struct MockSocketFactoryTwins {
    ip: SocketAddr,
    send: Sender<MockSocket>,
    recv: Option<Receiver<MockSocket>>,
}

impl MockSocketFactoryTwins {
    pub fn new(ip_one: SocketAddr, ip_two: SocketAddr) -> (Self, Self) {
        let (one_send, two_recv) = channel(100);
        let (two_send, one_recv) = channel(100);
        let one = MockSocketFactoryTwins {
            ip: ip_one,
            send: one_send,
            recv: Some(one_recv),
        };
        let two = MockSocketFactoryTwins {
            ip: ip_two,
            send: two_send,
            recv: Some(two_recv),
        };
        (one, two)
    }

    pub fn default_ip() -> (Self, Self) {
        let ip1 = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8080);
        let ip2 = ip1.clone();
        Self::new(ip1, ip2)
    }

    async fn get_connection_sender(&self) -> Sender<MockSocket> {
        self.send.clone()
    }
}

#[async_trait]
impl SocketFactory<MockSocketListener, MockSocket> for MockSocketFactoryTwins {
    async fn bind<A: ToSocketAddrs + Send + Sync>(
        &mut self,
        _addr: A,
    ) -> Result<MockSocketListener> {
        match self.recv.take() {
            Some(recv) => return Ok(MockSocketListener { recv }),
            None => {
                return Err(Error::new(
                    ErrorKind::NetworkError,
                    "Loopback mock can not listen to more than one socket",
                ))
            }
        }
    }

    async fn connect<A: ToSocketAddrs + Send + Sync>(&mut self, _addr: A) -> Result<MockSocket> {
        let (your_tx, my_rx) = channel(100);
        let (my_tx, your_rx) = channel(100);
        let mine = MockSocket {
            send: my_tx,
            recv: my_rx,
        };
        let yours = MockSocket {
            send: your_tx,
            recv: your_rx,
        };
        if let Err(_) = self.send.send(yours).await {
            panic!("Could not send new connection in loopback mock");
        }
        Ok(mine)
    }
}

impl SocketListener<MockSocket> for MockSocketListener {}

impl Stream for MockSocketListener {
    type Item = std::io::Result<MockSocket>;
    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        match self.recv.poll_recv(cx) {
            Poll::Ready(r) => match r {
                Some(socket) => return Poll::Ready(Some(Ok(socket))),
                //Since a TCP socket will never return None we will return a pending that violates
                //the future contract by not setting a waker. This only happens when no more
                //connections can arrive however.
                None => return Poll::Pending,
            },
            Poll::Pending => return Poll::Pending,
        }
    }
}

impl SocketStream for MockSocket {}

impl AsyncRead for MockSocket {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<std::io::Result<usize>> {
        let recv_buf = match self.recv.poll_recv(cx) {
            Poll::Ready(r) => match r {
                Some(buf) => buf,
                None => {
                    return Poll::Ready(Err(std::io::Error::new(
                        std::io::ErrorKind::NotConnected,
                        "Mock socket closed",
                    )))
                }
            },
            Poll::Pending => return Poll::Pending,
        };
        let min = std::cmp::min(recv_buf.len(), buf.len());
        for i in 0..min {
            buf[i] = recv_buf[i];
        }
        return Poll::Ready(Ok(min));
    }
}

impl AsyncWrite for MockSocket {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        let mut send_buf = [0; 1500];
        let min = std::cmp::min(buf.len(), send_buf.len());
        for i in 0..min {
            send_buf[i] = buf[i];
        }
        match self.send.poll_ready(cx) {
            Poll::Ready(r) => match r {
                Ok(_) => match self.send.try_send(send_buf) {
                    Ok(_) => {
                        return Poll::Ready(Ok(min));
                    }
                    Err(_e) => {
                        return Poll::Ready(Err(std::io::Error::from(
                            std::io::ErrorKind::BrokenPipe,
                        )))
                    }
                },
                Err(_e) => {
                    return Poll::Ready(Err(std::io::Error::from(std::io::ErrorKind::BrokenPipe)))
                }
            },
            Poll::Pending => return Poll::Pending,
        }
    }
    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<std::io::Result<()>> {
        Poll::Ready(Ok(()))
    }
    fn poll_shutdown(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<std::io::Result<()>> {
        Poll::Ready(Ok(()))
    }
}
